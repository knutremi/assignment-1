package com.knutremi.android.myfirstapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract.CommonDataKinds.Note;

public class Message {

	long id;
	String body;
	
	@SuppressWarnings("unused")
	private Message() {}
	
	public Message(final String body) {
		this.body = body;
	}
	
	public void save(DatabaseHelper dbHelper) {
		final ContentValues value = new ContentValues();
		value.put(BODY, this.body);
		
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
        this.id = db.insert(MESSAGE_TABLE_NAME, null, value);
        db.close();
	}
	
	public static Message getMessage(final DatabaseHelper dbHelper) {
		Message message;
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final Cursor c = db.query(MESSAGE_TABLE_NAME,
                        new String[] {BODY}, null, null, null, null, null);
        // make sure you start from the first item
        c.moveToFirst();
        while (!c.isAfterLast()) {
                final Message item = cursorToMessage(c);
            message = item;
            c.moveToNext();
        }
        // Make sure to close the cursor
        c.close();
        return new Message();
	}

	private static Message cursorToMessage(Cursor c) {
		
        final Message message = new Message();
        message.setBody(c.getString(c.getColumnIndex(BODY)));
        return message;

	}

	public void setBody(String body) {
		this.body = body;
	}
	
	public String getBody() {
		return body;
	}
	
	private static final String MESSAGE_TABLE_NAME = "messages";
	private static final String BODY = "body";
	public static final String MESSAGE_CREATE_TABLE = "CREATE TABLE "
			+ Message.MESSAGE_TABLE_NAME + " ("
             + Message.BODY + " TEXT"
             + ");";
}
